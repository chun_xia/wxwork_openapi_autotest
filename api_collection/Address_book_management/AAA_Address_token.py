"""
可以删除，token方法写在base_api文件里面了
"""


import env_config
from api_collection.base_api import BaseApi

host = env_config.run_env


class AddressToken(BaseApi):
    corpid = "ww6dc5ce3e1d98496c"
    corpsecret = 'g6C9nc6f2574h0NnzbVEie9qAh5qSCvZ7UEPqIWsM-U'

    def address_token(self):
        req = {
            "method": "get",
            "url": host + "/cgi-bin/gettoken",
            "params":
                {"corpid": self.corpid, "corpsecret": self.corpsecret}
        }
        r = self.request_http(req)
        return r


if __name__ == '__main__':
    pass
