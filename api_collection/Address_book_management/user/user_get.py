"""
Author: yx
Update at: 2022/11/16
info: 根据用户id获取用户的信息
"""

import env_config
from api_collection.base_api import BaseApi

host = env_config.run_env


class UserGet(BaseApi):
    access_token = BaseApi().address_token()

    def user_get(self, userid):
        req = {
            "method": "get",
            "url": host + "/cgi-bin/user/get",
            "params":
                {"access_token": self.access_token, "userid": userid}
        }
        r = self.request_http(req).json()
        return r


if __name__ == '__main__':
    pass
    # response = UserGet().user_get("YangXiong").json()
    # print(response)
