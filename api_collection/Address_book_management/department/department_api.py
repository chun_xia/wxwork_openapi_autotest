"""
Author: yx
Update at: 20222/4/19
info: 定义获取审批申请详情
"""

from api_collection.Address_book_management.AAA_Address_token import *

host = "https://qyapi.weixin.qq.com"
corpid = "ww6dc5ce3e1d98496c"
corpsecret = 'g6C9nc6f2574h0NnzbVEie9qAh5qSCvZ7UEPqIWsM-U'


class DepartmentApi(AddressToken):


    @staticmethod
    def get_department_accesstoken(env_corpid, env_corpsecret):
        path = "/cgi-bin/gettoken"
        url = host + path

        params = {"corpid": env_corpid,
                  "corpsecret": env_corpsecret}

        res = requests.get(url=url, params=params).json()
        access_token = res["access_token"]
        return access_token

    def create(self, name, name_en, parentid, order, id):
        """

        :param name: 是	部门名称。同一个层级的部门名称不能重复。长度限制为1~32个字符，字符不能包括:*?"<>｜
        :param name_en:否	英文名称。同一个层级的部门名称不能重复。需要在管理后台开启多语言支持才能生效。长度限制为1~32个字符，字符不能包括:*?"<>｜
        :param parentid:是	父部门id，32位整型
        :param order:否	在父部门中的次序值。order值大的排序靠前。有效的值范围是[0, 2^32)
        :param id:否	部门id，32位整型，指定时必须大于1。若不填该参数，将自动生成id
        :return:errcode	返回码
        err msg	对返回码的文本描述内容
        id	创建的部门id
        示例
        {
            "name": "广州研发中心",
            "name_en": "RDGZ",
            "parentid": 1,
            "order": 1,
            "id": 2
        }

        """
        path = "/cgi-bin/department/create?access_token={}".format(AddressToken.address_token())
        url = host + path
        data = {
            "name": name,
            "name_en": name_en,
            "parentid": parentid,
            "order": order,
            "id": id
        }

        res = requests.post(url=url, json=data).json()
        return res

    def simplelist(self, id=""):
        """
        :param id: 否	部门id。获取指定部门及其下的子部门（以及子部门的子部门等等，递归）。 如果不填，默认获取全量组织架构
        :return:
        """
        path = "/cgi-bin/department/simplelist?access_token={}".format(AddressToken.address_token())
        url = host + path
        params = {
            "id": id
        }

        res = requests.get(url=url, params=params).json()
        return res


# def get_approval_detail(env_host, access_token, sp_no):
#     path = "/cgi-bin/oa/getapprovalinfo?access_token={}".format(access_token)
#     url = env_host + path
#     data = {
#         "sp_no": sp_no
#     }
#     res = requests.post(url=url, json=data).json()
#     return res


if __name__ == '__main__':
    response = DepartmentApi().simplelist("")
    print(response)
    # assert response["errcode"] == 0
