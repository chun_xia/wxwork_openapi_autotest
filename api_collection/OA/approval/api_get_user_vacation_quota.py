"""
Author: yx
Update at: 2022/4/6
info: 定义获取成员假期余额接口，具体见：https://developer.work.weixin.qq.com/document/path/93376
"""


import requests
from testsuite_api.test_approval.common import oa_access_token, env_host


def get_user_vacation_quota(env_host, access_token, user_id):
    path = "/cgi-bin/oa/vacation/getuservacationquota?access_token={}".format(access_token)
    url = env_host + path
    data = {"userid": user_id}

    res = requests.post(url=url, json=data).json()
    return res


if __name__ == '__main__':
    response = get_user_vacation_quota(env_host, oa_access_token, "YangXiong")
    print(response)
    assert response["errcode"] == 0
