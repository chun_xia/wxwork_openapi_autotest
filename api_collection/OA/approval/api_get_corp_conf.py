"""
Author: yx
Update at: 20222/4/6
info: 定义获取企业假期管理配置接口
"""


import requests
from testsuite_api.test_approval.common import oa_access_token, env_host


def get_corp_conf(env_host, access_token):
    path = "/cgi-bin/oa/vacation/getcorpconf?access_token={}".format(access_token)
    url = env_host + path

    res = requests.post(url=url).json()
    return res


if __name__ == '__main__':
    response = get_corp_conf(env_host, oa_access_token)
    print(response)
    assert response["errcode"] == 0
