import requests

import env_config

host = env_config.run_env


class BaseApi:

    # req = {
    #     "method": "get",
    #     "url": "https://qyapi.weixin.qq.com/cgi-bin/gettoken",
    #     "params":
    #         {"corpid": "ww6dc5ce3e1d98496c", "corpsecret": "g6C9nc6f2574h0NnzbVEie9qAh5qSCvZ7UEPqIWsM-U"}
    # }

    def request_http(self, req):
        r = requests.request(**req)
        return r

    def address_token(self):
        corpid = "ww6dc5ce3e1d98496c"
        corpsecret = 'g6C9nc6f2574h0NnzbVEie9qAh5qSCvZ7UEPqIWsM-U'
        req = {
            "method": "get",
            "url": host + "/cgi-bin/gettoken",
            "params":
                {"corpid": corpid, "corpsecret": corpsecret}
        }
        # url = f'https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={self.corpid}&corpsecret={self.corpsecret}'
        # res = requests.get(url=url).json()
        r = self.request_http(req).json()
        return r["access_token"]
