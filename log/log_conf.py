from loguru import logger

logger.add("test_loguru_{time}.log")  # 在add定义输出的文件名
logger.add("test_loguru_{time}.log", rotation="1MB")   # 每个文件大小1MB.超过创建新文件
logger.add("test_loguru_{time}.log", rotation="12:00 ")   # 每天12点创建新文件
logger.add("test_loguru_{time}.log", retention="1week ")   # 日志保留一周
logger.debug("测试开始执行")
