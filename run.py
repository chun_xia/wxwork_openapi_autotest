"""
为什么和自己在终端执行的不一样
"""
import os
import sys

sys.path.append("D:\chunxiaProject\wxwork_openapi_autotest")


def run():
    os.system('pytest')


if __name__ == '__main__':
    run()
    # 定义报告存放路径
    # test_data_path = "./testreport_spot/allure_results"
    # test_report_path = './testreport_spot/allure_report'

    # 执行测试，生成报告
    # os.system(f'pytest ./testsuites_spot/test_usernum.py -s --alluredir={test_data_path}')
    # os.system(f'allure generate {test_report_path}/allure_results -o {test_report_path}/allure_report --clean')

    # 推送到lark群聊
    # url = 'https://open.larksuite.com/open-apis/bot/v2/hook/f075c078-e654-4c40-81a7-5507c41c8ddf'
    # push_report = "http://localhost:63342/spot_trade_http/testreport_spot/allure_report/index.html?_ijt=f1nv4bbbc3sa6ek1p406teas57&_ij_reload=RELOAD_ON_SAVE#"
    # msg = {"msg_type": "text", "content": {"text": f"测试执行成功，测试报告如下:{push_report}"}}
    # res = requests.post(url=url, json=msg).json()
    # # print(res)
    # if res['StatusCode'] == 0:
    #     logger.info('推送测试报告到lark成功')

    # f"测试执行成功，测试报告如下:{push_report}
