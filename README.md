# wxwork_openapi_autotest

#### 介绍
企业微信openApi自动化测试

python+requests+pytest+allure2

#### 框架特性
1、复用token
使用pytest的fixture特性，将token存储位session级别，用例批量执行只需获取一次token
ps：使用requests的session操作也可实现

2、用例并行执行
使用插件pytest-xdist，实现用例并行执行
手写多线程/线程池也可实现此操作

存在bug:
并行时需要使用锁，锁住复用的token函数才能保证获取token只执行一次，否则复用token特性会失效

3、测试入参参数化
使用pytest.mark.parametrize特性，实现入参的参数化传入


4、测试造数
死水-数据库数据或者有效等价类
本地活水，实时造数 方法1、列表生成器

5、封层封装框架，PO

#### 解决的痛点
框架分层，低耦合
api接口定义与测试用例解耦，测试用例与测试数据构造解耦，实现整体低耦合，易编写，易排查问题的效果

###轻松处理接口间调用关系
接口2的请求参数需要接口1的返回参数，使用api分开定义的方法+死水数据造数轻松理清接口间调用关系

###安全性校验：统一身份认证
每个模板都有统一身份校验，验证身份后才可以使用业务接口。 使用公共类抽取认证信息，进行缓存和判断

###用例前置（setup）与后置(teardown)


###全局日志设置

###连接数据库，对response的body进行业务逻辑校验
提供pymysql连接数据库demo，可自行参考
