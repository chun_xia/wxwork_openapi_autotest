# encoding:UTF-8

"""
author: yx
update at: 2022/04/18
info:定义公共固件
"""

import time

import pytest
from loguru import logger

# import pymysql

DATE_FORMAT = '%Y-%m-%d %H:%M:%S'

# # 默认作用域也是function
# @pytest.fixture(scope='function')
# def study_center():
#     study_center = pymysql.connect(host='xxx',
#                                    port=3309,
#                                    user='xxx',
#                                    database='study_center',
#                                    passwd='xxx')
#     cursor = study_center.cursor()
#     cursor.execute("SELECT 1+1")  # 测试数据库连接
#     values = cursor.fetchall()
#     assert values[0][0] == 2
#     print("数据库正常开启")
#
#     yield
#
#     study_center.close()
#     try:
#         cursor.execute("SELECT 1+1")
#     except pymysql.err.InterfaceError:
#         print("数据库正常关闭")


@pytest.fixture(autouse=True)
def timer_function_scope():
    logger.debug("测试开始执行")
    start = time.time()
    yield
    logger.debug("测试结束执行")
    print(' 单用例测试耗时: {:.3f}s'.format(time.time() - start))


@pytest.fixture(scope='session', autouse=True)
def timer_session_scope():
    start = time.time()
    print('测试开始时间: {}'.format(time.strftime(DATE_FORMAT, time.localtime(start))))

    yield

    finished = time.time()
    print('测试结束时间: {}'.format(time.strftime(DATE_FORMAT, time.localtime(finished))))
    print('测试总耗时: {:.3f}s'.format(finished - start))
