"""
Author: yx
Update at: 20222/4/6
info: 审批公用逻辑，access_token获取。需要参数：企业ID，审批应用secretID
"""
import requests


class EnvConfig(object):

    def __init__(self, env_host, env_corpid, env_corpsecret):
        self.env_host = env_host
        self.env_corpid = env_corpid
        self.env_corpsecret = env_corpsecret

    def login_config(self):
        path = "/cgi-bin/gettoken"
        url = self.env_host + path

        params = {"corpid": self.env_corpid,
                  "corpsecret": self.env_corpsecret}

        res = requests.get(url=url, params=params).json()
        access_token = res["access_token"]
        return access_token, self.env_host


# 在类外部定义一个函数，实现环境切换
def env(env_id):
    # 测试环境参数设置
    if env_id == 1:
        return EnvConfig("xxx", "xxx", "xxx").login_config()
    # 预发布环境参数设置
    elif env_id == 2:
        return EnvConfig("xxx", "xxx", "xxx").login_config()
    # 线上环境域名，企业ID，审批应用ID
    elif env_id == 3:
        return EnvConfig("https://qyapi.weixin.qq.com", "ww6dc5ce3e1d98496c",
                         "_7DFv4I_exN-axJdx0leRSXwA9lfxPNpPJ3bj2n1Hr0").login_config()


oa_access_token, env_host = env(3)

if __name__ == '__main__':
    print("OA子功能审批的access_token是{}".format(oa_access_token))
    print("执行用例的环境域名为是{}".format(env_host))
