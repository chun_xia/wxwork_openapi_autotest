"""
Author: yx
Update at: 20222/4/6
info: 测试获取成员假期余额接口
"""

from api_collection.OA.approval.api_get_user_vacation_quota import *
from testsuite_api.test_approval.common import oa_access_token, env_host


def test_1():
    res = get_user_vacation_quota(env_host, oa_access_token, "YangXiong")
    print(res)
    assert res['errcode'] == 0


def test_2():
    res = get_user_vacation_quota(env_host, oa_access_token, "杨")
    # print(res)
    assert res['errcode'] == 301063


if __name__ == '__main__':
    pass
