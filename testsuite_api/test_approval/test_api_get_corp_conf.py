"""
Author: yx
Update at: 20222/4/19
info: 测试获取企业假期管理配置
"""

from api_collection.OA.approval.api_get_corp_conf import *
from testsuite_api.test_approval.common import oa_access_token, env_host


def test_1():
    res = get_corp_conf(env_host, oa_access_token)
    # print(res)
    assert res['errcode'] == 0
    return res['errcode']


def test_2():
    a = test_1()
    print("a的值是:" + str(a))
    assert a == 0


if __name__ == '__main__':
    pass
